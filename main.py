# Save user data
from Welcome import welcome
from Calculate import calculate
import os, datetime


if __name__ == '__main__':

    welcome()
    user_name = input("Please enter your name: ")

    print("Please enter 2 numbers to perform operations!!!")
    x = int(input("Enter first number, x = "))
    y = int(input("Enter second number, y = "))

    # create dir
    try:
        os.mkdir("Hist")
    except:
        print("directory Hist exists already")

    # move to Hist dir
    os.chdir("Hist")

    # create file to fill all user data in mode "add" to create if it doesn't exists, and add data if it exists
    with open("history.txt", "a+") as f:
        f.write(f"Date and time : {datetime.datetime.now()} \n")
        f.write(f"User name: {user_name} \n")
        f.write("RESULT OF OPERATION PERFORM:\n")
        cal = calculate()
        f.write(f"ADDITION : {x} + {y} = {cal.add(x, y)} \n")
        f.write(f"SUBTRACTION : {x} - {y} = {cal.subtract(x, y)} \n")
        f.write(f"MULTIPLICATION : {x} x {y} = {cal.multiply(x, y)}\n")
        f.write(f"DIVISION : {x}/{y} = {cal.divide(x, y)} \n")
        f.write("************************************************************")

        # move insert to the first position and read all file content
        f.seek(0)
        files_lines = f.readlines()
        print(files_lines)
    f.close()





